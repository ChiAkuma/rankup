package polygondev.rankup.events;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import polygondev.rankup.init.rankup;

import java.sql.ResultSet;
import java.util.Date;

public class OnPlayerJoin implements Listener {

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        try {
            // Gibt geld von Vault zurück hat aber jetzt hier nichts zur sache
            /*
            RegisteredServiceProvider<Economy> economyProvider = rankup.plugin.getServer().getServicesManager().getRegistration(Economy.class);
            Economy eco = economyProvider.getProvider();
            double money = eco.getBalance(event.getPlayer());
            */
            
            //Erstellt eine Tabelle wenn es nicht existiert!
            rankup.sqlquery.execute("CREATE TABLE IF NOT EXISTS rankup (player_uuid varchar(255), expire varchar(255), dateOfBuy varchar(255), rank varchar(255))");
            
            //Wenn der Spieler mit der uuid nicht existiert wird catch aufgerufen. ansonsten wird abgefragt ob es expired ist
            try {
                //gibt die reihe mit der uuid vom spieler zurück falls keine da ist wird catch ausgeführt
                ResultSet cache = rankup.sqlquery.executeQuery("SELECT * from rankup WHERE player_uuid = '" + event.getPlayer().getUniqueId() + "'");
                cache.next();
                
                if (cache.getString("player_uuid").equals(event.getPlayer().getUniqueId() + "")) {
                    //hier wird abgefragt ob es expired ist oder nicht und sonstiges
                    Date date = new Date();
                }
            } catch (Exception ex) {
                
                Date date = new Date();
                rankup.sqlquery.executeUpdate("INSERT INTO rankup (player_uuid, expire, dateOfBuy, rank) VALUES ('" + event.getPlayer().getUniqueId() + "', '-1', '" + date + "', 'defaultrank');");
            }
       
        } catch (Exception e) {
            //irgend ein fehler ist entstanden
            rankup.console.sendMessage("[RankUp] ");
            
            //nicht in den offizielen builds enthalten
            e.printStackTrace();
        }
    }
    
}
