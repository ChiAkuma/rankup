package polygondev.rankup.init;

import com.huskehhh.mysql.mysql.MySQL;
import com.huskehhh.mysql.sqlite.SQLite;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;
import polygondev.rankup.commands.CMD_Rankup;
import polygondev.rankup.events.OnPlayerJoin;

import java.sql.Connection;
import java.sql.Statement;

public class rankup extends JavaPlugin {

    public static JavaPlugin plugin;
    public static Statement sqlquery;
    private Connection c;
    public static ConsoleCommandSender console;
    
    @Override
    public void onLoad() {
        plugin = this;
        console = plugin.getServer().getConsoleSender();
    }

    @Override
    public void onDisable() {
        try {
            c.commit();
        } catch (Exception e) {
            
        }
    }

    @Override
    public void onEnable() {
        //erstell command
        getCommand("rankup").setExecutor(new CMD_Rankup());
        
        //lädt die config oder erstellt diese
        FileConfiguration config = this.getConfig();
        console.sendMessage("[RankUp] Gestartet!!!");
        
        //setzt alle config defaults es sei denn sie existieren schon
        config.addDefault("SQL.type", "sqlite");
        config.addDefault("SQL.ip", "localhost");
        config.addDefault("SQL.port", 3306);
        config.addDefault("SQL.user", "root");
        config.addDefault("SQL.password", "password");
        config.addDefault("SQL.database", "database");
        
        config.addDefault("Ranks.defaultrank.expire", "10d");
        config.addDefault("Ranks.defaultrank.cost", 1000d);

        config.options().copyDefaults(true);
        saveConfig();
        
        //schaut ob mysql oder sqlite benutzt werden soll und erschafft eine verbindung
        //mysql
        if (!config.get("SQL.type").equals("sqlite")) {
            try {
                //MySQL MySQL = new MySQL(plugin, "host.name", "port", "database", "user", "pass");
                MySQL MySQL = new MySQL(
                        (String) (config.get("SQL.ip")),
                        (String) (config.get("SQL.port")),
                        (String) (config.get("SQL.database")),
                        (String) (config.get("SQL.user")),
                        (String) (config.get("SQL.pass"))
                );
                
                c = MySQL.openConnection();
                sqlquery = c.createStatement();
                
            } catch (Exception e) {
                console.sendMessage("[RankUp] Database connection error!");
            }
            //sqlite
        } else {
            try {
                SQLite sqLite = new SQLite("/test.db");
                c = sqLite.openConnection();
                sqlquery = c.createStatement();
                
            } catch (Exception e) {
                console.sendMessage("[RankUp] SQLite data error");
            }
        }
        
        //erstellt alle events
        plugin.getServer().getPluginManager().registerEvents(new OnPlayerJoin(), this);
        
    }
}
